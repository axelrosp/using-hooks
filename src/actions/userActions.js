export const ActionTypes = {
	ADD_USER: "ADD_USER",
	REMOVE_USER: "REMOVE_USER",
};
const addUser = (payload) => {
	return {
		type: ActionTypes.ADD_USER,
		payload: payload,
	};
};
const removeUser = (index) => {
	return {
		type: ActionTypes.ADD_USER,
		index: index,
	};
};

export default {
	addUser,
	removeUser,
};
