import React from "react";

function UserList({ users }) {
	return (
		<React.Fragment>
			{users.map((user) => {
				return <p>{user.name}</p>;
			})}
		</React.Fragment>
	);
}

export default UserList;
