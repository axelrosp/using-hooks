import React, { useState } from "react";

export default function Form({ submitValue }) {
	const [name, setName] = useState("");

	const handleSubmitValue = (event) => {
		event.preventDefault();
		submitValue({ name: name });
		setName("");
	};

	return (
		<React.Fragment>
			<form onSubmit={handleSubmitValue}>
				<div className="col-md-6 container-fluid">
					<div className="form-group row">
						<label className="col-sm-2">Name</label>
						<div className="col-sm-10">
							<input
								type="text"
								className="form-control"
								id="inputName"
								value={name}
								onChange={(e) => {
									setName(e.target.value);
								}}
							/>
						</div>
					</div>
					<button type="submit" className="col-sm-12 btn btn-primary mb-2 btn-success">
						Add value
					</button>
				</div>
			</form>
		</React.Fragment>
	);
}
