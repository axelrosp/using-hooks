import { ActionTypes } from "../actions/userActions";

//Array of users [{name: string}]
const initialState = [];

const userReducer = (state = initialState, action) => {
	switch (action.type) {
		case ActionTypes.ADD_USER:
			return [...state, action.payload];

		case ActionTypes.REMOVE_USER:
			const newState = [...state];
			newState.splice(action.index, 1);
			return {
				newState,
			};
		default:
			return state;
	}
};

export default userReducer;
