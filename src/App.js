import React from "react";
import logo from "./logo.svg";
import "./App.css";
import FormContainer from "./container/FormContainer";

function App() {
	return (
		<div className="App App-header">
			<div className="container">
				<FormContainer />
			</div>
		</div>
	);
}

export default App;
