import React from "react";
import Form from "../components/Form";
import { useSelector, useDispatch } from "react-redux";
import allActions from "../actions/userActions";
import UserList from "../components/UserList";

const FormContainer = () => {
	const { addUser } = allActions;
	const users = useSelector((state) => state.userReducer);
	const dispatch = useDispatch();

	const addUserHandler = (payload) => {
		dispatch(addUser(payload));
	};

	return (
		<React.Fragment>
			<Form submitValue={addUserHandler} />
			<UserList users={users} />
		</React.Fragment>
	);
};

export default FormContainer;
